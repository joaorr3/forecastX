﻿using System;
using System.Collections.Generic;
using System.Text;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.Helpers;
using forecastX.Xamarin.Models;
using forecastX.Xamarin.Services;
using forecastX.Xamarin.ViewModels;
using forecastX.Xamarin.LoginModels;


namespace forecastX.Xamarin.ViewModels
{
    public class MainViewModel
    {

        #region ViewModels

        //public LoginViewModel Login { get; set; }

        public Login Login { get; set; }

        public CityWeather CityWeather { get; set; }

        #endregion

        #region Properties

        public TokenResponse Token { get; set; }

        #endregion

        #region Constructures

        public MainViewModel()
        {
            this.Login = new Login();

            instance = this;
        }

        #endregion

        #region Singleton

        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }

        #endregion

    }
}
