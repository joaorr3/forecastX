﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using forecastX.Xamarin.ViewModels;

namespace forecastX.Xamarin.Infrastructure
{
    public class InstanceLocator
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructures

        public InstanceLocator()
        {
            this.Main = new MainViewModel();
        }

        #endregion

    }
}
