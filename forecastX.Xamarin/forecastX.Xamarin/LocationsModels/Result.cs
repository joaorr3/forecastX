﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace forecastX.Xamarin.LocationsModels
{
    public class Result
    {
        [JsonProperty("name")]
        public string name { get; set; }

    }
}
