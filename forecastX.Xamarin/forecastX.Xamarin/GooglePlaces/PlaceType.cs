
namespace forecastX.Xamarin.GooglePlaces
{
	public enum PlaceType
	{
		All,
		Geocode, 
		Address, 
		Establishment, 
		Regions, 
		Cities
	}

}
