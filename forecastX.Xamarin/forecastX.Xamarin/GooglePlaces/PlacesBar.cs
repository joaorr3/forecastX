
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace forecastX.Xamarin.GooglePlaces
{
	public delegate void PlacesRetrievedEventHandler(object sender, AutoCompleteResult result);

	public class PlacesBar : SearchBar
	{
		PlaceType placeType = PlaceType.Cities;

		LocationBias locationBias;

		Components components;

		string apiKey;

		int minimumSearchText;

		#region Property accessors
		public PlaceType Type
		{
			get
			{
				return placeType;
			}
			set
			{
				placeType = value;
			}
		}

		public LocationBias Bias
		{
			get
			{
				return locationBias;
			}
			set
			{
				locationBias = value;
			}
		}

		public Components Components
		{
			get
			{
				return components;
			}
			set
			{
				components = value;
			}
		}

		public string ApiKey
		{
			get
			{
				return apiKey;
			}
			set
			{
				apiKey = value;
			}
		}

		public int MinimumSearchText
		{
			get
			{
				return minimumSearchText;
			}
			set
			{
				minimumSearchText = value;
			}
		}
		#endregion

		public PlacesRetrievedEventHandler PlacesRetrieved;

		public PlacesBar()
		{
			TextChanged += OnTextChanged;
		}

		async void OnTextChanged(object sender, TextChangedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.NewTextValue) && e.NewTextValue.Length >= minimumSearchText)
			{
				var predictions = await GetPlaces(e.NewTextValue);
				if (PlacesRetrieved != null && predictions != null)
					PlacesRetrieved(this, predictions);
				else
					PlacesRetrieved(this, new AutoCompleteResult());
			}
			else
			{
				PlacesRetrieved(this, new AutoCompleteResult());
			}
		}

		async Task<AutoCompleteResult> GetPlaces(string newTextValue)
		{
			if (string.IsNullOrEmpty(apiKey))
			{
				throw new Exception(
					string.Format("You have not assigned a Google API key to PlacesBar"));
			}

			try 
			{
				var requestURI = CreatePredictionsUri(newTextValue);
				var client = new HttpClient();
				var request = new HttpRequestMessage(HttpMethod.Get, requestURI);
				var response = await client.SendAsync(request);

				if (!response.IsSuccessStatusCode)
				{
					Debug.WriteLine("PlacesBar HTTP request denied.");
					return null;
				}

				var result = await response.Content.ReadAsStringAsync();

				if (result == "ERROR")
				{
					Debug.WriteLine("PlacesSearchBar Google Places API returned ERROR");
					return null;
				}

				return JsonConvert.DeserializeObject<AutoCompleteResult>(result);
			} 
			catch (Exception ex)
			{
				Debug.WriteLine("PlacesBar HTTP issue: {0} {1}", ex.Message, ex);
				return null;
			}
		}

		string CreatePredictionsUri(string newTextValue)
		{
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
			var input = Uri.EscapeUriString(newTextValue);
			var pType = PlaceTypeValue(placeType);
			var constructedUrl = $"{url}?input={input}&types={pType}&key={apiKey}";

			if (locationBias != null)
				constructedUrl = constructedUrl + locationBias;
			if (components != null)
				constructedUrl += components;

			return constructedUrl;
		}

		string PlaceTypeValue(PlaceType type)
		{
			switch (type)
			{
				case PlaceType.All:
					return "";
				case PlaceType.Geocode:
					return "geocode";
				case PlaceType.Address:
					return "address";
				case PlaceType.Establishment:
					return "establishment";
				case PlaceType.Regions:
					return "(regions)";
				case PlaceType.Cities:
					return "(cities)";
				default:
					return "";
			}
		}

	}
}
