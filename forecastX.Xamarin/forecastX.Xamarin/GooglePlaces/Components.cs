﻿namespace forecastX.Xamarin.GooglePlaces
{
    public class Components
    {
        private string components;

        public Components(string components)
        {
            this.components = components;
        }

        public override string ToString()
        {
            return $"&components={components}";
        }
    }
}
