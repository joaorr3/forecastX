﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace forecastX.Xamarin.TabbedPages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		Main _main;

		public MainPage(Main main)
		{
			InitializeComponent ();

			_main = new Main();

			_main = main;


			temp.Text = _main.temp.ToString();

			pressure.Text = _main.pressure.ToString();

			humidity.Text = _main.humidity.ToString();

			temp_min.Text = _main.temp_min.ToString();

			temp_max.Text = _main.temp_max.ToString();

		}
	}
}