﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace forecastX.Xamarin.TabbedPages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CloudsPage : ContentPage
	{
		Clouds _clouds;

		public CloudsPage(Clouds clouds)
		{
			InitializeComponent ();

			_clouds = new Clouds();

			_clouds = clouds;

			all.Text = _clouds.all.ToString();

		}
	}
}