﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace forecastX.Xamarin.TabbedPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherPage : ContentPage
    {

        List<Weather> _weather;

        public int i;

        public WeatherPage(Weather[] weather)
        {
            InitializeComponent ();

            _weather = new List<Weather>();

            

            _weather = weather.ToList();

                
            main.Text = _weather[0].main.ToString();

            description.Text = _weather[0].description.ToString();


        }
    }
}