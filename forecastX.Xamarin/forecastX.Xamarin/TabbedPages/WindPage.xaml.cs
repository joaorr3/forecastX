﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace forecastX.Xamarin.TabbedPages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WindPage : ContentPage
	{
		Wind _wind;

		public WindPage(Wind wind)
		{
			InitializeComponent();

			_wind = new Wind();

			_wind = wind;

			speed.Text = _wind.speed.ToString();

			deg.Text = _wind.deg.ToString();


		}
	}
}