﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Services;
using forecastX.Xamarin.ViewModels;
using forecastX.Xamarin.Helpers;
using forecastX.Xamarin.LoginModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Settings;
using Plugin.Settings.Abstractions;


namespace forecastX.Xamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        //Login login;
        private ApiService apiService;
        UserLocal userLocal;

        public RegisterPage()
        {

            InitializeComponent ();

            //NavigationPage.SetHasBackButton(this, false);

            //login = new Login();
            apiService = new ApiService();
            

            RegisterButton.IsEnabled = false;

            RegisterButton.Clicked += RegisterButton_Clicked;
            EmailEntry.TextChanged += EmailEntry_TextChanged;
            PasswordEntry.TextChanged += PasswordEntry_TextChanged;
            ConfirmPasswordEntry.TextChanged += ConfirmPasswordEntry_TextChanged;
        }


        private void EmailEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (EmailEntry.Text != "" && PasswordEntry.Text != "" && ConfirmPasswordEntry.Text != "")
            {
                RegisterButton.IsEnabled = true;
            }
            else
            {
                RegisterButton.IsEnabled = false;
            }
        }

        private void PasswordEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PasswordEntry.Text != "" && EmailEntry.Text != "" && ConfirmPasswordEntry.Text != "")
            {
                RegisterButton.IsEnabled = true;
            }
            else
            {
                RegisterButton.IsEnabled = false;
            }
        }

        private void ConfirmPasswordEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PasswordEntry.Text != "" && EmailEntry.Text != "" && ConfirmPasswordEntry.Text != "")
            {
                RegisterButton.IsEnabled = true;
            }
            else
            {
                RegisterButton.IsEnabled = false;
            }
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {

            if (EmailEntry.Text == null || PasswordEntry.Text == null || ConfirmPasswordEntry.Text == null)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Verifique se introduziu todos os dados", "OK");
                return;
            }

            #region DoRegister

            Spinner.IsRunning = true;

            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert("Tente mais tarde", "Não há conexão à internet ", "OK");

                Spinner.IsRunning = false;

                return;
            }

            if (PasswordEntry.Text != ConfirmPasswordEntry.Text)
            {
                await Application.Current.MainPage.DisplayAlert("Tente Novamente", "A confirmação de password não coincide", "OK");

                Spinner.IsRunning = false;

                PasswordEntry.Text = string.Empty;
                ConfirmPasswordEntry.Text = string.Empty;

                return;
            }

            var token = await this.apiService.Register("https://forecastxapi20180525061042.azurewebsites.net/", EmailEntry.Text, PasswordEntry.Text, ConfirmPasswordEntry.Text);

            if (token != null)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Verifique os dados", "OK");

                Spinner.IsRunning = false;

                PasswordEntry.Text = string.Empty;
                ConfirmPasswordEntry.Text = string.Empty;

                return;

            }
            
            if (token == null)
            {

                userLocal = new UserLocal
                {
                    UserName = EmailEntry.Text,
                    Password = PasswordEntry.Text
                };

                string user = JsonConvert.SerializeObject(userLocal);

                Settings.GeneralSettings = user;

                await Application.Current.MainPage.DisplayAlert("Sucesso", "Faça login para entrar", "OK");

                Spinner.IsRunning = false;

                NameEntry.Text = string.Empty;
                EmailEntry.Text = string.Empty;
                PasswordEntry.Text = string.Empty;
                ConfirmPasswordEntry.Text = string.Empty;

                await Application.Current.MainPage.Navigation.PushAsync(new LoginPage(), true);
            }

            #endregion

        }

    }
}