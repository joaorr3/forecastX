﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Services;
using forecastX.Xamarin.ViewModels;
using forecastX.Xamarin.Helpers;
using forecastX.Xamarin.LoginModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Settings;
using Plugin.Settings.Abstractions;


namespace forecastX.Xamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        //Login login;
        private ApiService apiService;
        private UserLocal userLocal;

        public LoginPage ()
        {
            InitializeComponent ();

            apiService = new ApiService();

            NavigationPage.SetHasBackButton(this, false);


            LoginButton.IsEnabled = false;

            LoginButton.Clicked += LoginButton_Clicked;
            RegisterButton.Clicked += RegisterButton_Clicked;
            EmailEntry.TextChanged += EmailEntry_TextChanged;
            PasswordEntry.TextChanged += PasswordEntry_TextChanged;

            AboutButton.Clicked += AboutButton_Clicked;
            
        }

        //private async void _NewSearchCityPage()
        //{
        //    await this.Navigation.PushAsync(new SearchCityPage(), true);
        //}

        private void AboutButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.DisplayAlert("About", "Developed by João Ribeiro\nvr. 1.0", "OK");
        }

        private void EmailEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (EmailEntry.Text != "" && PasswordEntry.Text != "")
            {
                LoginButton.IsEnabled = true;
            }
            else
            {
                LoginButton.IsEnabled = false;
            }
        }

        private void PasswordEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PasswordEntry.Text != "" && EmailEntry.Text != "")
            {
                LoginButton.IsEnabled = true;
            }
            else
            {
                LoginButton.IsEnabled = false;
            }
        }

        private async void LoginButton_Clicked (object sender, EventArgs e)
        {

            #region DoLogin

            Spinner.IsRunning = true;

            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Não há conexão à internet ", "OK");

                Spinner.IsRunning = false;

                return;
            }

            var token = await this.apiService.GetToken("https://forecastxapi20180525061042.azurewebsites.net/", EmailEntry.Text, PasswordEntry.Text);

            if (token == null)
            {
                await Application.Current.MainPage.DisplayAlert("Erro2", "Email ou Password incorretos", "OK");

                Spinner.IsRunning = false;

                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                await Application.Current.MainPage.DisplayAlert("Erro3", token.ErrorDescription, "OK");

                Spinner.IsRunning = false;

                return;
            }

            Spinner.IsRunning = false;
            PasswordEntry.Text = string.Empty;

            //------------------------------------------------------------
            userLocal = new UserLocal
            {
                UserName = EmailEntry.Text,
                //Password = PasswordEntry.Text
                
            };

            EmailEntry.Text = string.Empty;

            string user = JsonConvert.SerializeObject(userLocal);

            Settings.GeneralSettings = user;
            //------------------------------------------------------------

            MainViewModel.GetInstance().Token = token;

            await Application.Current.MainPage.Navigation.PushAsync(new SearchCityPage(), true);

            #endregion

        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {

            #region DoRegister

            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage(), true);

            #endregion

        }

    }
}