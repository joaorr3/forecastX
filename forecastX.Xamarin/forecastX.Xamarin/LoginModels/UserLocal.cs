﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace forecastX.Xamarin.LoginModels
{
    public class UserLocal
    {
        //[PrimaryKey]
        //public int UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        //public override int GetHashCode()
        //{
        //    return UserId;
        //}
    }
}
