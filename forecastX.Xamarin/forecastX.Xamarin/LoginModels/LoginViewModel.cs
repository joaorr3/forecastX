﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using forecastX.Xamarin.Services;
using forecastX.Xamarin.ViewModels;
//using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace forecastX.Xamarin.LoginModels
{
    public class LoginViewModel : BaseViewModel
    {
                                                       
        //// V ISTO VERIFICA SE HOUVE ALGUMA ALTERAÇÃO FEITA V
        ////public event PropertyChangedEventHandler PropertyChanged;
        

        //#region Attributes
        private bool _isrunning;
        //private bool _isremembered;
        //private string _email;
        //private string _password;
        //private bool _isenabled;
        //private ApiService apiService;
        //#endregion


        //#region Properties
        //public string Email

        //{
        //    get { return _email; }
        //    set
        //    {
        //        SetValue(ref this._email, value);
        //    }
        //}

        //public string Password
        //{
        //    get { return _password; }
        //    set
        //    {
        //        SetValue(ref this._password, value);
        //    }
        //}

        //public bool IsEnabled
        //{
        //    get { return _isenabled; }
        //    set
        //    {
        //        SetValue(ref this._isenabled, value);
        //    }
        //}

        public bool IsRunning
        {
            get { return _isrunning; }
            set
            {
                SetValue(ref this._isrunning, value);
            }
        }

        //public bool IsRemembered
        //{
        //    get { return _isremembered; }
        //    set
        //    {
        //        if( _isremembered != value )
        //        {
        //            SetValue(ref this._isremembered, value);
        //        }
        //    }
        //}

        //#endregion


        //#region Constructors
        //public LoginViewModel()
        //{
        //    this.IsRemembered = true;
        //    apiService = new ApiService();
        //}
        //#endregion


        //#region Commands

        //public ICommand LoginCommand
        //{
        //    get
        //    {
        //        return new RelayCommand(Login);
        //    }
        //}

        //private async void Login()
        //{
        //    //var connection = await this.apiService.CheckConnection();

        //    //if (!connection.IsSucess)
        //    //{
        //    //    this.IsRunning = false;
        //    //    this.IsEnabled = true;
        //    //    await Application.Current.MainPage.DisplayAlert("Erro", "Email ou Password incorretos", "Ok!");

        //    //    return;
        //    //}

        //    //var token = await this.apiService.GetToken("http://app3api.azurewebsites.net/", this.Email,this.Password);

        //    //if (token == null)
        //    //{
        //    //    this.IsRunning = false;
        //    //    this.IsEnabled = true;

        //    //    await Application.Current.MainPage.DisplayAlert("Erro", "Email ou Password incorretos", "Ok!");

        //    //    return;
        //    //}

        //    //if (string.IsNullOrEmpty(token.AccessToken))
        //    //{
        //    //    this.IsRunning = false;
        //    //    this.IsEnabled = true;
        //    //    await Application.Current.MainPage.DisplayAlert("Erro", token.ErrorDescription, "Ok!");

        //    //    return;

        //    //}

        //    //MainViewModel.GetInstance().Token = token;


        //    //this.IsRunning = false;
        //    //this.IsEnabled = true;

        //    //this.Email = string.Empty;
        //    //this.Password = string.Empty;


        //    //MainViewModel.GetInstance().Countries = new CountriesViewModel();

        //    //await Application.Current.MainPage.Navigation.PushAsync(new CountriesPage());


        //    if (string.IsNullOrEmpty(this.Email))
        //    {
        //        await Application.Current.MainPage.DisplayAlert("Erro", "Insira o email", "Ok!");
        //        return;
        //    }

        //    if (string.IsNullOrEmpty(this.Password))
        //    {
        //        await Application.Current.MainPage.DisplayAlert("Erro", "Insira a password", "Ok!");
        //        return;
        //    }



        //    if (this.Email != "teste@email.com" || this.Password != "1234")
        //    {
        //        await Application.Current.MainPage.DisplayAlert("Erro", "Email ou Password incorretos", "Ok!");
        //        this.Password = string.Empty;
        //        this.IsRunning = false;

        //        return;
        //    }

        //    await Application.Current.MainPage.DisplayAlert("Boa", "Conseguimos", "Ok!");

        //    this.IsRunning = false;

        //    this.IsEnabled = true;

        //    this.Email = string.Empty;
        //    this.Password = string.Empty;

        //    MainViewModel.GetInstance().Countries = new CountriesViewModel();

        //    await Application.Current.MainPage.Navigation.PushAsync(new CountriesPage());

        //}


        //#endregion


    }
}