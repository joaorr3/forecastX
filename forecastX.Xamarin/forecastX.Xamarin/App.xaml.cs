using System;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.Helpers;
using forecastX.Xamarin.LoginModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Settings;
using Plugin.Settings.Abstractions;


[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace forecastX.Xamarin
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            UserLocal userLocal = JsonConvert.DeserializeObject<UserLocal>(Settings.GeneralSettings);

            if (Settings.GeneralSettings != "" && userLocal.UserName != "")
            {
                MainPage = new NavigationPage(new SearchCityPage());
            }
            else
            {
               MainPage = new NavigationPage(new LoginPage()); 
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
