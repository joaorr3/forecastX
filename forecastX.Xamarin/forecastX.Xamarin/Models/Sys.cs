﻿using System;
using System.Collections.Generic;
using System.Text;

namespace forecastX.Xamarin.Models
{
    public class Sys
    {
        public float type { get; set; }
        public float id { get; set; }
        public float message { get; set; }
        public string country { get; set; }
        public float sunrise { get; set; }
        public float sunset { get; set; }
    }
}
