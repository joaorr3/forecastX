﻿using System;
using System.Collections.Generic;
using System.Text;

namespace forecastX.Xamarin.Models
{
    public class Wind
    {
        public float speed { get; set; }
        public float deg { get; set; }
    }
}
