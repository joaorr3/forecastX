﻿using System;
using System.Collections.Generic;
using System.Text;

namespace forecastX.Xamarin.Models
{
    public class Coord
    {
        public float lon { get; set; }
        public float lat { get; set; }
    }
}
