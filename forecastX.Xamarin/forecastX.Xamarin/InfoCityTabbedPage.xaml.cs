﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.TabbedPages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace forecastX.Xamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoCityTabbedPage : TabbedPage
    {
        CityWeather _cityWeather;

        public InfoCityTabbedPage (CityWeather cityWeather, string city)
        {
            InitializeComponent();

            _cityWeather = new CityWeather();

            _cityWeather = cityWeather;

            this.Title = city;

            this.Children.Add(new MainPage(_cityWeather.main));

            this.Children.Add(new CloudsPage(_cityWeather.clouds));

            this.Children.Add(new WeatherPage(_cityWeather.weather));

            this.Children.Add(new WindPage(_cityWeather.wind));

        }

        

    }
}