﻿using System;
using System.Collections.Generic;
using System.Text;
using forecastX.Xamarin.Models;
using Newtonsoft.Json;

namespace forecastX.Xamarin.Data
{
    public class CityWeather
    {

        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "cod")]
        public int cod { get; set; }

        [JsonProperty(PropertyName = "base")]
        public string _base { get; set; }

        [JsonProperty(PropertyName = "visibility")]
        public int visibility { get; set; }

        [JsonProperty(PropertyName = "dt")]
        public int dt { get; set; }


        [JsonProperty(PropertyName = "coord")]
        public Coord coord { get; set; }

        [JsonProperty(PropertyName = "weather")]
        public Weather[] weather { get; set; }

        [JsonProperty(PropertyName = "main")]
        public Main main { get; set; }

        [JsonProperty(PropertyName = "wind")]
        public Wind wind { get; set; }

        [JsonProperty(PropertyName = "clouds")]
        public Clouds clouds { get; set; }

        [JsonProperty(PropertyName = "sys")]
        public Sys sys { get; set; }
        
    }
}
