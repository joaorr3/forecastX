﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.LoginModels;
using forecastX.Xamarin.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace forecastX.Xamarin.Services
{
    public class ApiService
    {
        public async Task<Response> CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your Internet Config."
                };
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable(
                "google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your Internet Connection."
                };
            }

            return new Response
            {
                IsSucess = true,
                Message = "Ok"
            };
        }


        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                

                var response = await client.PostAsync("Token", new StringContent(
                    String.Format("grant_type=password&userName={0}&password={1}", username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));

                var resultJSON = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJSON);

                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<TokenResponse> Register(string urlBase, string username, string password, string passwordConfirmation)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.PostAsync("api/account/register", new StringContent(
                    String.Format("Email={0}&Password={1}&ConfirmPassword={2}", username, password, passwordConfirmation),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));

                var resultJSON = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJSON);

                return result;
            }
            catch
            {
                return null;
            }
        }


        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
