﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.Models;
using Newtonsoft.Json;

namespace forecastX.Xamarin.Services
{
    public class GetWeatherCity
    {
        public string BaseUri { get; set; }
        public string AppId { get; set; }
        public string Q { get; set; }
        public string FullUriTest { get; set; }

        public GetWeatherCity()
        {
            BaseUri = "http://api.openweathermap.org";
            AppId = "48871935a6ca0f5fd51916eb35b3b822";
            //Q = "Lisboa";
            //FullUriTest = BaseUri + "q=" + Q + "&" + "appid=" + AppId;

        }

        //http://api.openweathermap.org/data/2.5/weather?q=Lisboa&appid=48871935a6ca0f5fd51916eb35b3b822

            
        public async Task<Response> GetCityInfo<T>(string city)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BaseUri);
                var url = "/data/2.5/weather?units=metric&" + "q=" + city + "&" + "appid=" + AppId;
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<CityWeather>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }


        }

    }
}
