﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.LocationsModels;
using forecastX.Xamarin.Models;
using Newtonsoft.Json;

namespace forecastX.Xamarin.Services
{
    public class GetLocation
    {
        public string BaseUri { get; set; }
        public string AppId { get; set; }
        public string Q { get; set; }
        public string FullUriTest { get; set; }

        public GetLocation()
        {
            BaseUri = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
            AppId = "AIzaSyCCD7aNl6LBs5mTjaSIE9I_2AKIH7JC90k";

        }

        //http://api.openweathermap.org/data/2.5/weather?q=Lisboa&appid=48871935a6ca0f5fd51916eb35b3b822

        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=39.010721,-9.285291&radius=500&types=(cities)&key=AIzaSyCCD7aNl6LBs5mTjaSIE9I_2AKIH7JC90k 



        public async Task<Response> GetCurrentLocation<T>(double lat, double lon)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BaseUri);
                var url = "?location="+ lat + "," + lon + "&radius=500&types=(cities)&key=" + AppId;
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<LocationModel>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }


        }


    }
}
