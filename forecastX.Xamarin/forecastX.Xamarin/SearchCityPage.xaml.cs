﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forecastX.Xamarin.Data;
using forecastX.Xamarin.LocationsModels;
using forecastX.Xamarin.GooglePlaces;
using forecastX.Xamarin.Helpers;
using forecastX.Xamarin.LoginModels;
using forecastX.Xamarin.Models;
using forecastX.Xamarin.Services;
using forecastX.Xamarin.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Plugin.Geolocator;

namespace forecastX.Xamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchCityPage : ContentPage
    {

        string GooglePlacesApiKey = "AIzaSyCCD7aNl6LBs5mTjaSIE9I_2AKIH7JC90k";

        GetWeatherCity getWeatherCity;
        GetLocation getLocation;

        public SearchCityPage()
        {
            InitializeComponent();

            getWeatherCity = new GetWeatherCity();
            getLocation = new GetLocation();

            NavigationPage.SetHasBackButton(this, false);

            UserLocal userLocal = JsonConvert.DeserializeObject<UserLocal>(Settings.GeneralSettings);

            Title = "Hello, " + userLocal.UserName + "!";

            

            search_bar.ApiKey = GooglePlacesApiKey;
            search_bar.Type = PlaceType.Cities;
            search_bar.PlacesRetrieved += Search_Bar_PlacesRetrieved;
            search_bar.TextChanged += Search_Bar_TextChanged;
            search_bar.MinimumSearchText = 2;
            results_list.ItemSelected += Results_List_ItemSelected;

            //GetLocation.Clicked += GetLocation_Clicked;

            LogoutButton.Clicked += LogoutButton_Clicked;
        }

        //private async void GetLocation_Clicked(object sender, EventArgs e)
        //{
        //    var locator = CrossGeolocator.Current;
        //    //locator.DesiredAccuracy = 50;
        //    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

        //    var lat = position.Latitude;
        //    var lon = position.Longitude;

        //    var location = await getLocation.GetCurrentLocation<LocationModel>(lat, lon);
        //    results_list.ItemsSource = (IEnumerable<LocationModel>)location.Result;
        //}


        void Search_Bar_PlacesRetrieved(object sender, AutoCompleteResult result)
        {
            results_list.ItemsSource = result.AutoCompletePlaces;
            spinner.IsRunning = false;
            spinner.IsVisible = false;
            

            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
                results_list.IsVisible = true;
        }

        void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                results_list.IsVisible = false;
                spinner.IsVisible = true;
                spinner.IsRunning = true;
            }
            else
            {
                results_list.IsVisible = true;
                spinner.IsRunning = false;
                spinner.IsVisible = false;
            }
        }

        async void Results_List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            spinner.IsRunning = true;
            spinner.IsVisible = true;

            if (e.SelectedItem == null)
                return;

            var prediction = (AutoCompletePrediction)e.SelectedItem;
            results_list.SelectedItem = null;

            var place = await Places.GetPlace(prediction.Place_ID, GooglePlacesApiKey);

            //if (place != null)
            //    await DisplayAlert(
            //        place.Name, string.Format("Lat: {0}\nLon: {1}", place.Latitude, place.Longitude), "OK");

            
            var selectedCity = await getWeatherCity.GetCityInfo<CityWeather>(place.Name);

            //MainViewModel.GetInstance().CityWeather = (CityWeather)selectedCity.Result;

            await Application.Current.MainPage.Navigation.PushAsync(new InfoCityTabbedPage((CityWeather)selectedCity.Result, place.Name), true);

            spinner.IsRunning = false;
            spinner.IsVisible = false;
        }

        private void LogoutButton_Clicked(object sender, EventArgs e)
        {
            //Application.Current.MainPage.Navigation.PushAsync(new LoginPage(), true);

            Settings.GeneralSettings = "";

            Application.Current.MainPage.Navigation.PopAsync(true);

            Application.Current.MainPage.Navigation.PushAsync(new LoginPage(), true);

            //Application.Current.MainPage.Navigation.RemovePage(Application.Current.MainPage.Navigation.NavigationStack[0]);
            //Application.Current.MainPage.Navigation.PopToRootAsync(true);

        }
    }
}