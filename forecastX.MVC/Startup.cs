﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(forecastX.MVC.Startup))]
namespace forecastX.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
